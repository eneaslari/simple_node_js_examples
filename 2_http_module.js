//The HTTP module can create an HTTP server that listens to server 
//ports and gives a response back to the client.
var http = require('http');
var url = require('url');
//create a server object:
http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.write('Hello World! from hhtp module'); //write a response to the client
  //res.write(req.url);//with the above link you will get /summer as a responce
					 //http://localhost:8080/summer
  //Split the Query String
  var q = url.parse(req.url, true).query;
  var txt = q.year + " " + q.month;
  res.end(txt);
}).listen(8080); //the server object listens on port 8080